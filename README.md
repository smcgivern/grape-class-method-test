A repository for testing out class methods and inheritance in Grape APIs.

I wanted to try out what happens if you use an intermediate base class
in front of `Grape::API` and `Grape::API::Instance`, similar to how
you'd have `ApplicationRecord` inherit from `ActiveRecord::Base` in a
Rails app. Note that according to [grape#1431]:

> These classes act like singletons to avoid any state

So even if we do this, we have to be very careful.

[grape#1431]: https://github.com/ruby-grape/grape/issues/1431

This app mounts four endpoints:

| Endpoint                    | Grape class            | Uses intermediate base class? |
| ---                         | ---                    | ---                           |
| `/ping`                     | `Grape::API`           | ❌                            |
| `/instance/ping`            | `Grape::API::Instance` | ❌                            |
| `/base_class/ping`          | `Grape::API`           | ✅                            |
| `/base_class_instance/ping` | `Grape::API::Instance` | ✅                            |

We use a class method defined in a base class to set a class-level
`@annotation` attribute, which we then send in one of the headers
`X-Annotation` or `X-Annotation-Instance` if present.

This seems to work OK, but actually fetching the class (to get the
class's instance variables) is a little painful - hence the two headers.

1. For `Grape::API`, we can use `route.app.options[:for].base` to get
   the class that's responsible for this API endpoint. This is in
   `X-Annotation`.
2. For `Grape::API::Instance`, it's just `route.app.options[:for]` (no
   `#base`), and that's in `X-Annotation-Instance`.

Putting it all together, we can test it out:

```shell
$ for x in {/,/instance/,/base_class/,/base_class_instance/}; do echo $x; curl -si "http://localhost:9292${x}ping" | grep X-Annotation; echo ''; done
/
X-Annotation: none
X-Annotation-Instance: none

/instance/
X-Annotation: none
X-Annotation-Instance: none

/base_class/
X-Annotation: BaseClassAPI
X-Annotation-Instance: none

/base_class_instance/
X-Annotation: none
X-Annotation-Instance: BaseClassInstanceAPI
```
