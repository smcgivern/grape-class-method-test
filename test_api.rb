require 'grape'

class InstanceAPI < Grape::API::Instance
  get 'ping' do
    "pong\n"
  end
end

class Base < Grape::API
  class << self
    def annotate(value)
      @annotation = value
    end
  end
end

class BaseInstance < Grape::API::Instance
  class << self
    def annotate(value)
      @annotation = value
    end
  end
end

class BaseClassAPI < Base
  annotate 'BaseClassAPI'

  get 'ping' do
    "pong\n"
  end
end

class BaseClassInstanceAPI < BaseInstance
  annotate 'BaseClassInstanceAPI'

  get 'ping' do
    "pong\n"
  end
end

class TestAPI < Grape::API
  before do
    header 'X-Annotation', route.app.options[:for].base.instance_variable_get(:@annotation) || 'none'
    header 'X-Annotation-Instance', route.app.options[:for].instance_variable_get(:@annotation) || 'none'
  end

  mount InstanceAPI => '/instance'
  mount BaseClassAPI => '/base_class'
  mount BaseClassInstanceAPI => '/base_class_instance'

  get 'ping' do
    "pong\n"
  end
end
